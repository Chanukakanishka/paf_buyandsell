import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient) { } 
  
  HOMEURL='http://localhost:3000/users'
  createUser(user){
    return this.http.post(this.HOMEURL,user)

  }
  validateUser(obj){
    return this.http.post(this.HOMEURL+'/validateuser',obj)
  }
}

