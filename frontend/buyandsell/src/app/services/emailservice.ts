import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class Emailservice {

  constructor(private http:HttpClient) { } 
  
  HOMEURL='http://localhost:3000/sendmail'
  sendmail(email){
    return this.http.post(this.HOMEURL,email)

  }
  
}