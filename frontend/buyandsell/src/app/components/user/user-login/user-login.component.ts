import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UserService } from "src/app/services/user.service";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-user-login",
  templateUrl: "./user-login.component.html",
  styleUrls: ["./user-login.component.scss"]
})
export class UserLoginComponent implements OnInit {
  constructor(
    private router: Router,
    private userservice: UserService,
    private toaster: ToastrService
  ) {}
  tempuser = {
    email: "",
    password: ""
  };
  ngOnInit() {}
  openSignupUser() {
    this.router.navigate(["RegisterUser"]);
  }
  signIn() {
   // console.log(this.tempuser);
    this.userservice.validateUser(this.tempuser).subscribe((res: any) => {
      console.log(res);
      if (res.code == 0) {
        if (res.result.length > 0) {
          this.toaster.success("User Login Sucess..");
          this.router.navigate([""]);
        } else {
          this.toaster.error("Invaild User");
        }
      } else {
        this.toaster.error("Network Error");
      }
    });
  }
}
