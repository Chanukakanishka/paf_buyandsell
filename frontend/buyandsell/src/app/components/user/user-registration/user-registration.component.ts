import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/usermodel';
import { UserService } from 'src/app/services/user.service';
import { ToastrService } from 'ngx-toastr';
import { Emailservice } from 'src/app/services/emailservice';
import { Router } from '@angular/router';


@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.scss']
})
export class UserRegistrationComponent implements OnInit {

  user={}as User
  confirmPassword=''
  constructor(private userservice:UserService,private toaster: ToastrService,private emailservice:Emailservice, private router:Router) { }

  ngOnInit() {
  }

  signupUser(){

    var regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    if(this.user.email==''){
      this.toaster.error('Email is Empty') 
    }else if(!regexp.test(this.user.email)){
      this.toaster.error("Email is Invalid")
    
    }else if(this.user.Name==''){
      this.toaster.error('First Name is Empty')
    }else if(this.user.catagory==''){
      this.toaster.error('Select a Catagory')
    }else if(this.user.Address==''){
      this.toaster.error('Address is Empty')
    }else if(this.user.PhoneNumber==''){
      this.toaster.error('Phone Number is Empty')
    }else if(this.user.password==''){
      this.toaster.error('Password is Empty')
    }else if(this.confirmPassword==''){
      this.toaster.error('Confirm Password is Empty')
    }else if (this.user.type==''){
      this.toaster.error('Please Select the Catagory')
    
    }else if(this.user.password!=this.confirmPassword){
       this.toaster.error('Password is not matched')
    }else{
      console.log(this.user)
    this.userservice.createUser(this.user).subscribe((res:any)=>{
      console.log(res)
      if(res.code ==0){
        this.toaster.success('User Rigistered Sucessfully..please check your email...')
        this.router.navigate([""]);
        var obj = {
          email:this.user.email
        }
        this.emailservice.sendmail(obj).subscribe((resp:any)=>{
          console.log(resp)
        })
      }else{
        this.toaster.error('Invalid User Registration..')
      }
    })

    }

    
  }
}
