import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { UserLoginComponent } from "./components/user/user-login/user-login.component";
import { UserRegistrationComponent } from "./components/user/user-registration/user-registration.component";

import { HttpClientModule } from "@angular/common/http";
import { ToastrModule } from "ngx-toastr";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { from } from "rxjs";
import { HeaderComponent } from "./components/landing-page/header/header.component";
import { PublishAddComponent } from "./components/landing-page/publish-add/publish-add.component";
import { TestcComponent } from "./components/landing-page/testc/testc.component";
import { StepOneComponent } from "./components/landing-page/stepper/step-one/step-one.component";
import { StepTwoComponent } from "./components/landing-page/stepper/step-two/step-two.component";
import { StepperComponent } from "./components/landing-page/stepper/stepper/stepper.component";
import { MainComponent } from "./components/main/main.component";

@NgModule({
  declarations: [
    AppComponent,
    UserLoginComponent,
    UserRegistrationComponent,
    HeaderComponent,
    PublishAddComponent,
    TestcComponent,
    StepOneComponent,
    StepTwoComponent,
    StepperComponent,
    MainComponent
  ],
  imports: [
    FormsModule,
    BrowserAnimationsModule,

    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
