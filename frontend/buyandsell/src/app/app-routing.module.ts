import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { UserLoginComponent } from "./components/user/user-login/user-login.component";
import { UserRegistrationComponent } from "./components/user/user-registration/user-registration.component";
import { MainComponent } from "./components/main/main.component";

const routes: Routes = [
  {
    path: "",
    component: MainComponent
  },
  {
    path: "RegisterUser",
    component: UserRegistrationComponent
  },
  {
    path: "loginuser",
    component: UserLoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
