const seq = require('sequelize');
const db = require('../helper/dbconnection');

const Producers = db.seq.define('producer',
    {
        email: {
            type: seq.STRING
        },
        C_name: {
            type: seq.STRING
        },
        catagory: {
            type: seq.STRING
        },
        Address: { 
            type : seq.STRING
        },
        phoneNumber: {
            type: seq.STRING
        },
        
        password: {
            type:seq.STRING
        }


    },
    {
        timestamps: false
    }
);

Producers.sync({force:false});
module.exports = Producers;