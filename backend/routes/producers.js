var express = require('express');
const userService=require('../services/producerService');
var router = express.Router();

/* GET users listing. */
router.get('/producers', function(req, res, next) {
  producerService.GetAllProducers.then((data)=>{
    res.send(data)

  })
});
router.get('/:id', function(req, res, next) {
 producerService.FindProducerById(req.params.id).then((data)=>{
    res.send(data)

  })
});
router.post('/', function(req, res, next) {
  producerService.CreateProducer(req.body).then((data)=>{
    res.send(data)

  })
});

router.post('/validateproducer', function(req, res, next) {
  producerService.ValidateProducer(req.body).then((data)=>{
    res.send(data)

  })
});

module.exports = router;